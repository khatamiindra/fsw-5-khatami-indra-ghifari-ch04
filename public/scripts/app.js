class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.buttonSubmitData = document.getElementById("submitFilter");
    this.tipeDriver = document.getElementById("tipeDriver");
    this.dateSewa = document.getElementById("dateSewa");
    this.waktuJemput = document.getElementById("waktuJemput");
    this.jumlahPenumpang = document.getElementById("jumlahPenumpang");
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.buttonSubmitData.onclick = this.run;
    this.run
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add('col-3', 'cars-card');
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);

    this.buttonSubmitData.addEventListener('click', async(e) => { 
      let tipeDriverVal = this.tipeDriver.options[this.tipeDriver.selectedIndex].value 
      let dateSewaVal = this.dateSewa.value 
      let waktuJemputVal = this.waktuJemput.value 
      let jumlahPenumpangVal = this.jumlahPenumpang.value
      // console.log(tipeDriverVal, dateSewaVal, waktuJemputVal, jumlahPenumpangVal);

      this.clear()
      let cars = await Binar.listCars((car) => {
        let result = true;

        let dateTime = dateSewaVal + "T" + waktuJemputVal;

        let dateCar = dateSewaVal[9]
        
        // Filter by Date, Time and Passenger
        if((!isNaN(Date.parse(dateTime))) && (!isNaN(parseInt(jumlahPenumpangVal)))) {
        result = (car.availableAt <= Date.parse(dateTime)) && (car.capacity >= parseInt(jumlahPenumpangVal));
       }

       // Filter by Date and Time only
        if(!isNaN(Date.parse(dateTime))) {
        result = (car.availableAt <= Date.parse(dateTime))
        }

        // Filter by Passenger only
        if(!isNaN(parseInt(jumlahPenumpangVal))){
        result = (car.capacity >= parseInt(jumlahPenumpangVal))
        }

        // Filter by Date only
        if(!isNaN(parseInt(dateCar))){
          result = car.availableAt.getDate() === parseInt(dateCar)
        }
        
        return result;
      })

      console.log(cars)

      Car.init(cars);
    })
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
