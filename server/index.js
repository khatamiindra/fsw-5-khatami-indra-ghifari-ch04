const http = require ('http');
const path = require ('path');
const fs = require ('fs');
const PORT = 3000
const PUBLIC_DIRECTORY = path.join(__dirname, `../public`);

function getHTML(htmlFileName){
    const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName)
    return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
    switch(req.url) {
      case "/":
        res.writeHead(200, {'Content-Type':'text/html'})
        res.end(getHTML("index.html"))
        return;
      case "/cars":
        res.writeHead(200, {'Content-Type':'text/html'})
        res.end(getHTML("cars.html"))
        return;
        default:
          if((req.url.indexOf('.css') != -1) || (req.url.indexOf('.jpg') != -1) || (req.url.indexOf('.png') != -1) || (req.url.indexOf('.js') != -1)){ 
            const cssFilePath = path.join(PUBLIC_DIRECTORY, `${req.url}`) 
            fs.readFile(cssFilePath, function (err,data){ 
                res.write(data); 
                res.end(); 
            }); 
          }
          else{ 
            res.writeHead(404) 
            res.end("File Not Found") 
          } 
    }
}    

  const server = http.createServer(onRequest);

  server.listen(PORT, 'localhost', () => {
    console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
  })